// Select DOM Elements
const btnMenu = document.getElementById('btn-menu');
const menu = document.getElementById('nav');
const nameTabs = document.querySelectorAll('.text');
const contactInfos = document.querySelectorAll('.contact-info');

// OPEN MENU
function toggleMenu() {
    menu.classList.toggle('visible');
}

btnMenu.addEventListener('click', toggleMenu);

// SHOW CONTACT INFO
nameTabs.forEach((tab) => {
    
    tab.addEventListener('mouseenter', () => {
        tab.querySelector('.contact-info').classList.add('show-contact');
    });   
    tab.addEventListener('mouseleave', () => {
        tab.querySelector('.contact-info').classList.remove('show-contact');
    });

});